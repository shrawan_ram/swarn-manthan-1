<?php

use App\Http\Controllers\admin\WinController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest', 'namespace' => 'admin'], function () {
    Route::any('/', 'UserController@index')->name('admin_login');
    Route::post('main/checklogin', 'UserController@checklogin');
});



Route::group(['middleware' => 'auth:admin', 'namespace' => 'admin'], function () {

    // Dashboard
    Route::get('home', 'DashboardController@index')->name('admin_home');

    // User 
    // Route::resource('user', 'UsersController');
    Route::post('user/delete', 'UsersController@destroyAll');
    Route::post('state/delete', 'StateController@destroyAll');
    Route::post('city/delete', 'CityController@destroyAll');
    Route::post('role/delete', 'RoleController@destroyAll');
    Route::post('marchant/delete', 'UsersController@destroyAll');
    Route::get('user_list', 'UsersController@userlist');
    Route::get('enquiry/marchant', 'EnqueryController@marchantEnquiry');
    Route::get('enquiry/user', 'EnqueryController@userEnquiry');
    Route::post('offer/delete', 'OfferController@destroyAll');
    Route::post('product/delete', 'ProductController@destroyAll');
    Route::post('category/delete', 'CategoryController@destroyAll');
    Route::get('product/changeStatus', 'ProductController@changeproductStatus');
    Route::get('product/remove-image', 'ProductController@removeProductimage');

    // Master 
    Route::resources([
        'page'         => 'PagesController',
        'state'         => 'StateController',
        'city'         => 'CityController',
        'role'         => 'RoleController',
        'offer'         => 'OfferController',

        'marchant'      => 'UsersController',
        'product'      => 'ProductController',
        'category'      => 'CategoryController',
    ]);
    Route::get('marchant/status/{user}', 'UsersController@changestatus')->name('marchantchangestatus');
    Route::get('product/status/{user}', 'ProductController@changestatus')->name('productchangestatus');

    // page 
    Route::post('page/delete', 'PagesController@destroyAll');

    // setting 
    Route::resource('setting', 'SettingController');


    Route::get('logout', 'UserController@logout')->name('admin_logout');
    Route::get('general-setting', 'SettingController@edit')->name('general_setting');
});
