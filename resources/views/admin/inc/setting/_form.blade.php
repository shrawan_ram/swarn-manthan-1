@if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
     <button type="button" class="close" data-dismiss="alert">x</button>
     {{$message}}
   </div>
@endif

@if(count($errors->all()))
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
@endif
<div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            {{Form::label('title', 'Website Title')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder'=>'Enter Website Title','id'=>'title','required'=>'required'])}}
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            {{Form::label('tagline', 'Website Tagline')}}
            {{Form::text('tagline', '', ['class' => 'form-control', 'placeholder'=>'Enter Website Tagline','id'=>'tagline','required'=>'required'])}}
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            {{Form::label('email', 'Email')}}
            {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Enter Email','id'=>'email','required'=>'required'])}}
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            {{Form::label('mobile', 'Mobile No.')}}
            {{Form::number('mobile', '', ['class' => 'form-control', 'placeholder'=>'Enter Mobile No.','id'=>'mobile','required'=>'required'])}}
          </div>
        </div>
        <div class="col-lg-12">
          <h5>Gold Detail :-</h5>
        </div>
        <div class="col-lg-4">
          <div class="form-group">
            {{Form::label('gold_price', 'Gold Price')}}
            {{Form::number('gold_price', '', ['class' => 'form-control', 'placeholder'=>'Enter Gold Price','id'=>'gold_price','required'=>'required'])}}
          </div>
        </div>
        <div class="col-lg-4">
          <div class="form-group">
            {{Form::label('gold_weight', 'Gold Weight')}}
            {{Form::text('gold_weight', '', ['class' => 'form-control', 'placeholder'=>'Enter Gold Weight','id'=>'gold_weight','required'=>'required'])}}
          </div>
        </div>
        @php
          $goldArr = [
          '' => 'Select Unit',
          'gm' => 'Gm',
          'kg' =>  'Kg'
          ];
        @endphp
        <div class="col-lg-4">
          <div class="form-group">
            {{Form::label('gold_unit', 'Gold Unit')}}
            {{Form::select('gold_unit', $goldArr,'', ['class' => 'form-control','id'=>'gold_unit','required'=>'required'])}}
          </div>
        </div>
        <div class="col-lg-12">
          <h5>Silver Detail :-</h5>
        </div>
        <div class="col-lg-4">
          <div class="form-group">
            {{Form::label('silver_price', 'Silver Price')}}
            {{Form::number('silver_price', '', ['class' => 'form-control', 'placeholder'=>'Enter Silver Price','id'=>'silver_price','required'=>'required'])}}
          </div>
        </div>
        <div class="col-lg-4">
          <div class="form-group">
            {{Form::label('silver_weight', 'Silver Weight')}}
            {{Form::text('silver_weight', '', ['class' => 'form-control', 'placeholder'=>'Enter Silver Weight','id'=>'silver_weight','required'=>'required'])}}
          </div>
        </div>
        @php
          $silverArr = [
          '' => 'Select Unit',
          'gm' => 'Gm',
          'kg' =>  'Kg'
          ];
        @endphp
        <div class="col-lg-4">
          <div class="form-group">
            {{Form::label('silver_unit', 'Select Silver Unit')}}
            {{Form::select('silver_unit', $silverArr,'', ['class' => 'form-control','id'=>'silver_unit','required'=>'required'])}}
          </div>
        </div>
           
  </div>
