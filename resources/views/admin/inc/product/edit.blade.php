<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
       @include('admin.common.TopHeader')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Product</h6>
                  <a href="{{ url('admin/product') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> View Product</a>
                </div>

                 <!-- Card Body -->
                <div class="card-body">
                {!! Form::open(['method' => 'PUT', 'route'=>array('product.update', $edit['id']), 'class' => 'user', 'files'=>'true']) !!}
                      @include('admin.inc.product._form')
                      <div class="row">
                      <div class="col-lg-6">
                      @if (!$productimage->isEmpty())
                        <h4>Product Images</h4>
                        <div id="remove-image">
                        
                        <table style="border:1px solid #d2d6de;width:100%">
                            <tr class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">Image</th>
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">status</th>
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">action</th>
                            </tr>
                            @foreach($productimage as $key=>$ci)
                            <tr class="text-center">
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                                  <img src="{{ url('imgs/product/'.$ci->name) }}" width="250"> 
                                </td>
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;;">
                                    <input data-id="{{$ci->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" {{ $ci->status ? 'checked' : '' }}>
                                </td>
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                                    <a class="btn-danger btn-floating remove-image" data-id="{{ $ci->id }}"><i class="fas fa-cross text-white p-1"></i></a>
                                </td>
                                
                            </tr>
                            @endforeach
                        </table>
                          <!--<div class="row">-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--</div>-->
                        
                        </div>
                        @else 
                        @endif
                      </div>
                      <div class="col-lg-6">
                      <div class="form-group">
                          {{Form::label('image', 'Select Product Image')}}
                          {{Form::file('image', ['class' => 'form-control', 'placeholder'=>'Enter Product Image','id'=>'image'])}}
                        </div>
                        <img src="{{ url('imgs/product/'.$edit->image) }}" alt="" width="250"><br>
                      
                      {{Form::label('images', 'Multiple Image')}}
                      <div class="input-group control-group increment mb-3" >
                        <input type="file" name="images[]" class="form-control">
                        <div class="input-group-btn"> 
                          <button class="btn btn-success input-add" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                        </div>
                      </div>
                      <div class="clone hide mb-3">
                        <div class="control-group input-group" style="margin-top:10px; margin-bottom:10px">
                          <input type="file" name="images[]" class="form-control">
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger input-remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>
                      </div>
                      </div>
                  <div class="text-right">
                    <input type="submit"class="btn btn-primary" value="Edit Product"/>
                  </div>
                {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
         </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
