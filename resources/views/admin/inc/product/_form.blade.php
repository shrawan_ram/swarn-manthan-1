@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

<div class="row">

  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('name', 'Enter Product Name')}}
      {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Enter Product Name','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('slug', 'Enter Slug')}}
      {{Form::text('slug', '', ['class' => 'form-control', 'placeholder'=>'Enter slug','id'=>'title'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('shop_id', 'Select Shop')}}
      {{Form::select('shop_id', $shopArr,'', ['class' => 'form-control','id'=>'shop_id','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('category_id', 'Select Category')}}
      {{Form::select('category_id', $catArr,'', ['class' => 'form-control','id'=>'category_id','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('material', 'Enter Matarial')}}
      {{Form::text('material', '', ['class' => 'form-control', 'placeholder'=>'Enter material','id'=>'material'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('product_weight', 'Enter Product Weight')}}
      {{Form::text('product_weight', '', ['class' => 'form-control', 'placeholder'=>'Enter product weight','id'=>'product_weight'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('diamond_weight', 'Enter Diomond Weight')}}
      {{Form::text('diamond_weight', '', ['class' => 'form-control', 'placeholder'=>'Enter diomond weight','id'=>'diamond_weight'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('size', 'Enter Size')}}
      {{Form::text('size', '', ['class' => 'form-control', 'placeholder'=>'Enter size','id'=>'size'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('price', 'Enter Product Price')}}
      {{Form::text('price', '', ['class' => 'form-control', 'placeholder'=>'Enter Product Price','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  
  <div class="col-lg-12">
    <div class="form-group">
      {{Form::label('description', 'Enter description')}}
      {{Form::textarea('description', '', ['class' => 'form-control','id'=>'description', 'placeholder'=>'Enter short description','rows'=>'4', 'col'=>'3'])}}
    </div>
  </div>
</div>