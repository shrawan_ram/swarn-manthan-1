<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Enquery extends Model
{
    protected $guarded  = [];
    
    protected $with  = ['user','shop'];

    public function enquery_product()
    {
        return $this->hasOne('App\Model\EnqueryProduct', 'enquery_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function shop()
    {
        return $this->hasOne('App\Model\Shop', 'id', 'shop_id');
    }
}
