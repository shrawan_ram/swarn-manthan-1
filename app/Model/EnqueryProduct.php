<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EnqueryProduct extends Model
{
    protected $guarded  = [];
    
    protected $with  = ['user'];

    public function enquery()
    {
        return $this->hasOne('App\Model\Enquery', 'id', 'enquery_id');
    }
    public function category()
    {
        return $this->hasOne('App\Model\Category', 'id', 'category_id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
        
    }
}
