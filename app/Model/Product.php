<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded  = [];
    
    protected $with  = ['user','shop'];

    public function category()
    {
        return $this->hasOne('App\Model\Category', 'id', 'category_id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function shop()
    {
        return $this->hasOne('App\Model\Shop', 'id', 'shop_id');
    }
    
}
