<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Enquery;
use Illuminate\Http\Request;
use Validator;

use App\Model\Notification;
use App\Model\Product;
use App\Model\Shop;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
	public function index(Request $request)
	{
		$lists = Category::latest()->get();

		if ($lists->isEmpty()) {
			$re = [
				'status' => false,
				'message'	=> 'No record(s) found.'
			];
		} else {
			$re = [
				'status' => true,
				'message'	=> $lists->count() . " records found.",
				'data'   => $lists
			];
		}

		return response()->json($re);
	}

	public function product(Category $id)
	{
		$category = $id;
		$lists = Product::latest()->with('shop')->where('category_id', $category->id)->get();

		$re = [
			'status' 	=> true,
			'data'   	=> $lists
		];

		return response()->json($re);
	}
}
