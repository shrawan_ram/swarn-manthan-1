<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Enquery;
use Illuminate\Http\Request;
use Validator;

use App\Model\Notification;
use App\Model\Shop;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
	public function index(Request $request)
	{
		$user = Auth::user();
		$shop = Shop::where('user_id', $user->id)->firstOrFail();

		$lists = Notification::latest()->where('shop_id', $shop->id)->get();
		foreach ($lists as $key => $list) {
			$enquery = Enquery::with('enquery_product')->findOrFail($list->enquery_id);
			$list->enquery_detail = $enquery;
		}

		if ($lists->isEmpty()) {
			$re = [
				'status' => false,
				'message'	=> 'No record(s) found.'
			];
		} else {
			$re = [
				'status' => true,
				'message'	=> $lists->count() . " records found.",
				'data'   => $lists
			];
		}

		return response()->json($re);
	}

	public function show(Notification $notification)
	{
		$lists = $notification;
		$enquery = Enquery::with('enquery_product')->findOrFail($lists->enquery_id);
		$lists->enquery_detail = $enquery;

		$re = [
			'status' 	=> true,
			'data'   	=> $lists
		];

		return response()->json($re);
	}
}
