<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Model\Role;
use App\Model\Shop;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = User::with('role')->where('role_id', 3)->get();
        // dd($lists);
        $page  = 'user.list';
        $title = 'Marchant list';
        $data  = compact('lists','page','title');
        return view('admin.layout',$data);
    }
    public function userlist()
    {
        $lists = User::with('role')->where('role_id', 2)->get();
        // dd($lists);
        $page  = 'user.user_list';
        $title = 'User list';
        $data  = compact('lists','page','title');
        return view('admin.layout',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page  = "user.add";
        $title = "Marchant Add";

        $city = City::get();
        $cityArr = [
            '' => 'Select City'
        ];
        foreach($city as $c){
            $cityArr[$c->id] = $c->name;
        }
        $edit='';
        
        $data  = compact('page', 'title', 'request','cityArr','edit');
        return view('admin.layout',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record.name'         => 'required',
            'mobile'       => 'required|unique:users',
            'email'        => 'required|unique:users',
            'record1.city_id'      => 'required',
            'record1.name'      => 'required',
            'record1.pincode'      => 'required',
            'record1.address'      => 'required',
        ];
        

        $request->validate($rules);
        $input = $request->record;
        $input['role_id'] =  '3';
        $input['mobile'] =  $request->mobile;
        $input['email'] =  $request->email;
        // $input['password'] =  Hash::make($request->record['password']);
        
        $obj = new User($input);

        $obj->save();

        $input1 = $request->record1;
        $input1['user_id'] =  $obj->id;
        
        $obj1 = new Shop($input1);

        $obj1->save();

        return redirect(route('marchant.index'))->with('success', 'Success! New record has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $marchant)
    {
        $edit = User::findOrFail($marchant->id);
        $shop = Shop::where('user_id', $marchant->id)->first();
        $request->replace($edit->toArray());   
        $editData =  ['record' => $edit->toArray(), 'record1' => $shop->toArray()];
        $request->replace($editData); 
        $request->flash();
        
        $city = City::get();
        $cityArr = [
            '' => 'Select City'
        ];
        foreach($city as $c){
            $cityArr[$c->id] = $c->name;
        }

        $page  = 'user.edit';
        $title = 'User Edit';
        $data  = compact('page', 'title','edit','request','cityArr');

        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $marchant)
    {
        $rules = [
            'record.name'         => 'required',
            'email'        => 'required|unique:users,email,'.$marchant->id,
            'mobile'       => 'required|unique:users,mobile,'.$marchant->id,
            'record1.city_id'      => 'required',
            'record1.name'      => 'required',
            'record1.pincode'      => 'required',
            'record1.address'      => 'required',
        ];
        $request->validate($rules);

        $obj =  User::findOrFail($marchant->id);
        $input = $request->record;
        // if($request->record['password'] != '' || $request->record['password'] != null) {
        //     $input['password'] =  Hash::make($request->record['password']);
        // }else {
        //     $input['password'] =  $marchant->password;
        // }
        $input['mobile'] =  $request->mobile;
        $input['email'] =  $request->email;
        $obj->update($input);

        $obj1 = Shop::where('user_id', $marchant->id)->first();
        $input1 = $request->record1;
        $obj1->update($input1);

        return redirect(route('marchant.index'))->with('success', 'Success!.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        // dd($request);
        $ids = $request->sub_chk;
        // dd($ids);
        User::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }

    public function changestatus(Request $request, User $user)
    {
        // dd($user);
        $user->status = $request->status;
        $user->save();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
